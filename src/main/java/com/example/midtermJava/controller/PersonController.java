package com.example.midtermJava.controller;

import com.example.midtermJava.entity.Person;
import com.example.midtermJava.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class PersonController {
    protected PersonService personService;

    @GetMapping("/api/v2/users/")
    public ResponseEntity<?> getPersons(){
        return ResponseEntity.ok ( personService.findAll() );
    }
    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> savePerson(@RequestBody Person person){
        return ResponseEntity.ok ( personService.create( person ) );
    }
    @PutMapping("/api/v2/users/")
    public ResponseEntity<?> updatePerson(@RequestBody Person person){
        return ResponseEntity.ok ( personService.update( person ) );
    }
    @DeleteMapping("/api/v2/users/{id}")
    public void deletePerson(@PathVariable Long id){
        personService.delete ( id );
    }
}
